import React, { Component } from "react";
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView } from "react-native";
import firebase from '../../config'

export default class SearchUser extends Component {

    static navigationOptions = () => {
        return {
            title: ``
        }
    }

    constructor(...props){
        super(...props)
        this.state = { name: '', email: '', photoUrl: '', users: [] }
        const user = firebase.auth().currentUser
        console.warn(user.email)
    }

    componentDidMount() {
    }

    render() {
        return (
            <ScrollView>
                <View style={styles.container} >
                    <View style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', flexWrap: 'wrap', width: '100%', padding: 8, marginTop: 20 }} >
                        <View style={{ borderColor: 'rgba(0,0,0,0.2)', width: '90%', padding: 10, borderRadius: 10, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 3 }} >
                            <View style={{ padding: 10, flexDirection: 'row', justifyContent: 'space-around' }} >
                                <View style={{ width: 100, alignItems: 'center', }}>
                                    <Image source={require('../../assets/pikachu.png')} style={{ width: 80, height: 80 }} resizeMode="stretch" />
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', fontWeight: 'bold', textAlign: 'center' }} >
                                    <Text style={{ padding: 5 }} >Nombre del Usuario</Text>
                                    <TouchableOpacity style={{ backgroundColor: '#673AB7', borderRadius: 30 }}  >
                                        <Text style={{ color: '#fff', padding: 4, width: 80, textAlign: 'center' }} >Ver perfil</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View>
                    <Text>{this.state.name}</Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: { flex: 1, alignItems: 'center', justifyContent: 'center' }
})