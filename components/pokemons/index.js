import React, { Component } from "react";
import { View, Text, ToolbarAndroid, StyleSheet, SafeAreaView, ActivityIndicator, ScrollView, TouchableOpacity, StatusBar } from "react-native";
import PokemonsList from "./PokemonsList";
import Icon from 'react-native-vector-icons/MaterialIcons'
import firebase from '../../config'
const BASE_URL = 'https://pokeapi.co/api/v2/pokemon/'

export default class Pokemons extends Component {
    static navigationOptions = {
        header: null
    }

    state = { pokemons: [], loading: true, next: null, previous: null, urls: [], ids: [] }

    componentDidMount() {
        this.getPokemons(BASE_URL)
    }

    getPokemons = async (url) => {
        const pokemons = await fetch(url)
        const json = await pokemons.json()
        this.setState({ pokemons: json.results, loading: false, next: json.next, previous: json.previous })
    }

    handleNext = () => {
        const { next } = this.state
        if (next != null) {
            this.setState({ loading: true })
            this.getPokemons(next)
        }
    }

    handlePrevious = () => {
        const { previous } = this.state
        if (previous != null) {
            this.setState({ loading: true })
            this.getPokemons(previous)
        }
    }

    handleShowDetail = (name) => {
        this.props.navigation.navigate('Detail', { name: name })
    }

    render() {
        return (
            <SafeAreaView>
                <StatusBar barStyle="light-content" backgroundColor="#c62828" animated={true} />
                <View style={styles.banner} >
                    <Text style={styles.titleBanner} >Pokedex</Text>
                </View>
                <ScrollView >
                    <View>
                        {
                            (this.state.loading)
                                ?
                                <View style={styles.container} >
                                    <Text style={styles.title} >cargando pokemons</Text>
                                    <ActivityIndicator size="large" color="#2196F3" />
                                </View>
                                :
                                <View>
                                    <PokemonsList ids={this.state.ids} pokemons={this.state.pokemons} handleShowDetail={this.handleShowDetail} />
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }} >
                                        <View style={styles.boxes} >
                                            <TouchableOpacity onPress={this.handlePrevious} style={{ padding: 2, alignItems: 'center' }} >
                                                <Icon name="navigate-before" color="#c62828" size={30} style={{ borderRadius: 40, borderColor: '#c62828', borderWidth: 1 }} />
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={this.handleNext} style={{ padding: 2, alignItems: 'center' }} >
                                                <Icon name="navigate-next" color="#c62828" size={30} style={{ borderRadius: 40, borderColor: '#c62828', borderWidth: 1 }} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                        }

                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: { height: 400 },
    banner: { height: 40, backgroundColor: '#c62828', justifyContent: 'center' },
    titleBanner: { fontSize: 18, fontFamily: 'Roboto',  textAlign: 'center', fontWeight: 'bold', color: '#fff', padding: 30 },
    title: { fontSize: 14, textAlign: 'center', marginTop: 10, padding: 20 },
    boxes: { flexDirection: 'row', flexWrap: 'wrap', margin: 6, justifyContent: 'space-around', padding: 8, marginBottom: 40 },
})