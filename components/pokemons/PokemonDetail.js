import React, { Component } from "react";
import { View, Text, ActivityIndicator, Image, TouchableOpacity, Alert, YellowBox, TextInput, ProgressBarAndroid, ScrollView } from "react-native";
import { Overlay } from "react-native-elements";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
//firebase
import firebase from '../../config'
//pokemonDetail
import { pokemonDetail } from "./pokemons.style";
YellowBox.ignoreWarnings(['Setting a timer'])
export default class PokemonDetail extends Component {

    constructor(...props) {
        super(...props)
        this.ref = firebase.firestore().collection('my_pokemons')
        this.state = {
            pokemon: [], name: null, loading: true, isVisible: false, id_pokemon: null,
            id_user: null, nickname: '', loadingCapture: false
        }
    }

    static navigationOptions = () => {
        return {
            title: `Datos`
        }
    }


    handleSavePokemon = () => {
        if (this.state.nickname == "" ) {
            this.ref.add({
                id_user: this.state.id_user,
                id_pokemon: this.state.pokemon.id,
                name: this.state.pokemon.name,
                nickname: this.state.nickname,
                image: `https://pokeres.bastionbot.org/images/pokemon/${this.state.pokemon.id}.png`
            }).then(() => {
                Alert.alert('Pokemon capturado!', 'Puedes verlo en tus pokemons', [
                    { text: 'Aceptar', onPress: () => { 
                        this.setState({loadingCapture: false})
                        this.handleReturnHome()
                     } }
                ])
            })
                .catch(error => console.warn(error))
        } else {
            Alert.alert('¡Espera!', 'Ponle un nombre a tu pokemon', [
                { text: 'Aceptar' }
            ])
        }
    }

    handleReturnHome = () => {
        this.setState({ isVisible: false })
        this.props.navigation.navigate('Home')
    }

    async componentDidMount() {
        await this.setState({ name: this.props.navigation.getParam('name') })
        const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${this.state.name}/`)
        const json = await response.json()
        const user = firebase.auth().currentUser
        this.setState({ pokemon: json, loading: false, id_user: user.uid })
    }

    onPressSave = () => {
        this.setState({ isVisible: true })
    }

    render() {
        return (
            <ScrollView>
                <View style={pokemonDetail.container} >
                    <View style={pokemonDetail.containerImage} >
                        <Image
                            style={pokemonDetail.image}
                            source={{ uri: `https://pokeres.bastionbot.org/images/pokemon/${this.state.pokemon.id}.png` }}
                            resizeMode="stretch"
                        />
                    </View>
                    <Text style={pokemonDetail.title} >{this.state.pokemon.name}</Text>
                    {
                        (this.state.loading)
                            ?
                            <View style={pokemonDetail.containerLoader} >
                                <Text style={pokemonDetail.titleModal} >cargando información</Text>
                                <ActivityIndicator size="large" color="#2196F3" />
                            </View>
                            :
                            <View>
                                <Text style={{ textAlign: 'center' }} >Datos</Text>
                                <View style={{ alignItems: 'center' }}>
                                    <Text>XP: {this.state.pokemon.base_experience}</Text>
                                    <Text>Peso: {this.state.pokemon.weight} kg</Text>
                                    <Text>Altura: {(this.state.pokemon.height / 10)} m</Text>
                                </View>
                                <View style={pokemonDetail.containerStat} >
                                    {
                                        this.state.pokemon.stats.map(stat => (
                                            <View key={stat.stat.name} style={pokemonDetail.containerStats} >
                                                {
                                                    (stat.stat.name == 'speed') ? <Text style={pokemonDetail.titleStats} >Velocidad</Text>
                                                        : (stat.stat.name) == 'special-defense' ? <Text style={pokemonDetail.titleStats} >Defensa Especial</Text>
                                                            : (stat.stat.name) == 'special-attack' ? <Text style={pokemonDetail.titleStats} >Ataque Especial</Text>
                                                                : (stat.stat.name) == 'defense' ? <Text style={pokemonDetail.titleStats} >Defensa</Text>
                                                                    : (stat.stat.name) == 'attack' ? <Text style={pokemonDetail.titleStats} >Ataque</Text>
                                                                        : <Text style={pokemonDetail.titleStats} >HP</Text>
                                                }
                                                <ProgressBarAndroid
                                                    styleAttr="Horizontal" indeterminate={false} color="#d32f2f"
                                                    progress={(stat.base_stat / 100)} style={{ width: 150 }}
                                                />
                                            </View>
                                        ))

                                    }
                                </View>
                                <View style={pokemonDetail.containerBoxes} >
                                    <View style={pokemonDetail.favouriteBox} >
                                        <Text style={pokemonDetail.titleFav} >Añadir a Favoritos</Text>
                                        <TouchableOpacity onPress={() => this.onPressSave()} >
                                            <Text style={{ textAlign: 'center' }} >
                                                <Icon name="star-outline" size={20} color="#F9A825" />
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View>
                                    <Overlay isVisible={this.state.isVisible} height={280} width={220}
                                    >
                                        {
                                            this.state.loadingCapture == false
                                                ?
                                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >
                                                    <Text style={pokemonDetail.titleOverlay} >¡ Nombra tu pokemon !</Text>
                                                    <View style={pokemonDetail.inputContainer} >
                                                        <TextInput
                                                            underlineColorAndroid="transparent" placeholder="Nombre del Pokemon"
                                                            keyboardType="default" style={{ textAlign: 'center' }}
                                                            value={this.state.pokemon.name}
                                                            onChangeText={nickname => this.setState({ nickname: nickname })} />
                                                    </View>
                                                    <TouchableOpacity style={{ margin: 10 }} onPress={() => {
                                                        this.setState({loadingCapture: true})
                                                        this.handleSavePokemon()}
                                                        } >
                                                        <Text style={pokemonDetail.textButton} >Capturar</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity style={{ position: 'absolute', bottom: 10 }} onPress={() => this.setState({ isVisible: false })} >
                                                        <Icon name="close" size={30} />
                                                    </TouchableOpacity>
                                                </View>
                                                :
                                                <View style={pokemonDetail.containerLoader} >
                                                    <Text style={pokemonDetail.titleModal} >capturando</Text>
                                                    <ActivityIndicator size="large" color="#b71c1c" />
                                                </View>
                                        }
                                    </Overlay>
                                </View>
                            </View>
                    }
                </View>
            </ScrollView>
        )
    }
}