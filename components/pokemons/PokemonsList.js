import React from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { pokemonList } from "./pokemons.style";
import { Overlay } from "react-native-elements";

const PokemonsList = (props) => (
    <View style={pokemonList.container} >
        <View style={pokemonList.boxes} >
            {
                props.pokemons.map((pokemon, i) => (
                    <View key={pokemon.name} style={pokemonList.borderBox} >
                        <View style={pokemonList.containerImage} >
                            <Image
                                style={pokemonList.image}
                                source={{ uri: `https://pokeres.bastionbot.org/images/pokemon/${pokemon.url.split('/')[pokemon.url.split('/').length - 2]}.png` }}
                                resizeMode="stretch"
                            />
                            
                        </View>
                        <Text style={pokemonList.title} >{pokemon.name}</Text>
                        <TouchableOpacity onPress={() => { props.handleShowDetail(pokemon.name) }} >
                            <Text style={{ textAlign: 'center', padding: 10 }} >
                                <Icon name="pokeball" size={30} color="#c62828" />
                            </Text>
                        </TouchableOpacity>
                    </View>
                ))
            }
        </View>
    </View>
)

export default PokemonsList