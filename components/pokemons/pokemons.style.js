import { StyleSheet } from "react-native";

const pokemonDetail = StyleSheet.create({
    containerLoader: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    container: { flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'stretch' },
    banner: { height: 90, backgroundColor: '#c62828' },
    titleBanner: { fontSize: 18, textAlign: 'center', paddingVertical: 30, fontWeight: 'bold', color: '#fff' },
    title: { fontSize: 15, textAlign: 'center', textTransform: 'uppercase', letterSpacing: 4, color: '#616161', fontWeight: 'bold', padding: 10 },
    titleFav: { fontSize: 12, textAlign: 'center', letterSpacing: 4, color: '#616161', fontWeight: 'bold', padding: 10 },
    titleModal: { fontSize: 13, textAlign: 'center', textTransform: 'uppercase', letterSpacing: 4, color: '#616161', fontWeight: 'bold', padding: 30 },
    image: { width: 100, height: 100, alignItems: 'center', padding: 4, marginTop: 4 },
    borderButton: { height: 60, width: 20, paddingTop: 20, borderRadius: 4, borderWidth: 0.2, borderColor: '#2196F3', marginTop: 10 },
    containerBoxes: { marginTop: 10, height: 100, display: 'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' },
    favouriteBox: { borderRadius: 4, borderWidth: 0.3, borderColor: '#2196F3', width: 200, height: 80, marginTop: 8 },
    inputContainer: { borderRadius: 10, borderColor: '#1E88E5', borderWidth: 2, margin: 5, width: 200 },
    containerImage: { display: 'flex', justifyContent: 'center', alignItems: 'center' },
    containerStats: { display: 'flex', justifyContent:'space-around' , flexDirection: 'row' },
    titleStats: { padding: 2, marginRight: 2, fontSize: 14, width: 120, textAlign: 'right' },
    titleOverlay: { fontSize: 15, fontWeight: 'bold', padding: 10 },
    textButton: { padding: 10, backgroundColor: '#009688', borderRadius: 40, color: '#fff' },
    containerStat: { display: 'flex', flexDirection: 'column', alignItems: 'center', padding: 4 }
})

const pokemonList = StyleSheet.create({
    container: { flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' },
    boxes: { display: 'flex', flexDirection: 'row', flexWrap: 'wrap', margin: 2, justifyContent: 'space-around' },
    image: { width: 100, height: 100, alignItems: 'center' },
    title: { fontSize: 10, textAlign: 'center', textTransform: 'uppercase', letterSpacing: 2, color: '#616161', fontWeight: 'bold', padding: 10 },
    borderBox: { borderRadius: 4, borderWidth: 0.8, borderColor: '#f44336', marginTop: 4, width: 110, marginTop: 8 },
    containerImage: { display: 'flex', justifyContent: 'center', alignItems: 'center', margin: 2, padding: 2 }
})

export { pokemonDetail, pokemonList }