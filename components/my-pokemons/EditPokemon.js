import React from 'react'
import { View, Text, StyleSheet } from "react-native"
import { Overlay } from "react-native-elements"

const EditPokemon = ( props ) => (
    <Overlay isVisible={props.isVisible}  >
        <View>
            <Text>Edita la Información del Pokemon</Text>
        </View>
    </Overlay>
)

export default EditPokemon