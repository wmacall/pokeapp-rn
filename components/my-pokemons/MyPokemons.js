import React, { Component } from 'react'
import { View, TextInput, Text, StyleSheet, Image, ActivityIndicator, Alert, TouchableOpacity, Share, ScrollView, YellowBox, KeyboardAvoidingView } from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Overlay } from "react-native-elements";
//firebase
import firebase from '../../config'
import MyPokemonsList from './MyPokemonsList';
YellowBox.ignoreWarnings(['Setting a timer'])
export default class MyPokemons extends Component {
    _isMounted = false
    constructor(...props) {
        super(...props)
        const user = firebase.auth().currentUser
        this.ref = firebase.firestore().collection("my_pokemons").where("id_user", "==", user.uid)
        this.unsubscribe = null
        this.state = {
            loading: true,
            pokemons: [],
            isVisible: false,
            newName: ''
        }
    }

    onCollectionUpdate = (querySnapshot) => {
        let pokemons = []
        querySnapshot.forEach((doc) => {
            const { id_pokemon, name, nickname, image } = doc.data()
            pokemons.push({
                key: doc.id,
                doc,
                id_pokemon, name, nickname, image
            })
        })
        if (this._isMounted) {
            this.setState({
                pokemons,
                loading: false
            })
        }
    }

    componentDidMount() {
        this._isMounted = true
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate)
    }

    componentWillUnmount() {
        this._isMounted = false
    }


    handleSingOut = () => {
        firebase.auth().signOut()
        this.props.navigation.navigate('LoginScreen')
    }

    onShare = (pokemonName) => {
        Share.share({
            message: `Mira este pokemon! https://pokemondb.net/pokedex/${pokemonName}`
        }, {
                dialogTitle: 'Poke App'
            })
    }

    handleDelete = (key, pokemon) => {
        Alert.alert('Eliminar Pokemon', `¿Estas seguro que deseas eliminar a ${pokemon}?`, [
            {
                text: 'Si', onPress: () => {
                    firebase.firestore().collection('my_pokemons').doc(key).delete()
                        .catch((error) => console.warn(error))
                }
            }, { text: 'Cancelar' }
        ])
    }

    render() {
        return (
            <View>
                <View style={{ position: 'absolute', backgroundColor: '#c62828', height: 40, top: 0, marginBottom: 20, width: '100%', justifyContent: 'center', alignItems: 'center' }} >
                    <Text style={{ fontSize: 15, padding: 6, color: '#fff' }} >Mis Pokemons</Text>
                </View>
                <ScrollView>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around', flexWrap: 'wrap', backgroundColor: '#fff', marginBottom: 8, marginTop: 40 }} >
                        {
                            this.state.pokemons.map(pokemon => (
                                <View key={pokemon.key} style={{ margin: 4, borderRadius: 20 }}>
                                    <View style={{ alignItems: 'center', padding: 6 }} >
                                        <Text style={{ fontSize: 10, color: '#616161', textTransform: 'uppercase', fontWeight: '600' }} >{pokemon.name}</Text>
                                        <Text style={{ maxWidth: 100, textAlign: 'center' }} >{pokemon.nickname}</Text>
                                    </View>
                                    <View style={{ alignItems: 'center', marginBottom: 50 }} >
                                        <Image
                                            source={{ uri: pokemon.image }}
                                            style={{ backgroundColor: '#fff', height: 160, width: 160 }} resizeMode="stretch" />
                                    </View>
                                    <View style={{ backgroundColor: '#e53935', width: '100%', borderBottomRightRadius: 20, borderBottomLeftRadius: 20, height: 30, justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: -2, flexDirection: 'row' }} >
                                        <TouchableOpacity onPress={() => { this.setState({ isVisible: true }) }} style={{ padding: 10 }} >
                                            <Icon name="circle-edit-outline" color="#fff" size={20} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => console.warn('Ok')} style={{ padding: 10 }} >
                                            <Icon name="share-outline" color="#fff" size={20} />
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            ))
                        }
                    </View>
                    <Overlay isVisible={this.state.isVisible} height={300} width={240} animated={true} animationType="fade" >
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >
                            <Text style={{ position: 'absolute', fontSize: 16, padding: 8, top: 0 }} >Edita tu Pokemon</Text>
                            <KeyboardAvoidingView style={{ alignItems: 'center' }} >
                                <Text style={{ maxWidth: 200, textAlign: 'center', padding: 4 }} >
                                    Cambiar el nombre
                                </Text>
                                <View style={styles.inputContainer} >
                                    <TextInput
                                        underlineColorAndroid="transparent" placeholder="Nuevo mote"
                                        keyboardType="default" style={styles.inputText} style={{ textAlign: 'center' }}
                                        onChangeText={newName => this.setState({ newName: newName })} />
                                </View>
                                <TouchableOpacity style={{ alignItems: 'center', padding: 10, backgroundColor: '#009688', maxWidth: 40, borderRadius: 60, marginTop: 4 }} >
                                    <Icon name="check" color="#fff" size={20} />
                                </TouchableOpacity>
                            </KeyboardAvoidingView>
                            <TouchableOpacity onPress={() => this.setState({ isVisible: false })} style={{ position: 'absolute', padding: 4, bottom: 0 }} >
                                <Icon name="close-circle-outline" color="#757575" size={30} />
                            </TouchableOpacity>
                        </View>
                    </Overlay>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputContainer: { borderRadius: 10, borderColor: '#1E88E5', borderWidth: 2, margin: 5, width: 200 },
})