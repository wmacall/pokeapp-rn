import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, Alert } from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const MyPokemonsList = (props) => (
    <View style={styles.container}>
        <View style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', margin: 2, justifyContent: 'space-between' }} >
            {
                props.pokemons.map((pokemon, i) => (
                    <View key={pokemon.id_pokemon} style={{ borderRadius: 30, borderColor: '#1565C0', borderWidth: 0.4, padding: 18, alignItems: 'center', margin: 3 }} >
                        <Icon name="pokeball" size={30} color="#c62828" />
                        <Image
                            source={{ uri: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id_pokemon}.png` }}
                            resizeMode='stretch' style={{ width: 80, height: 80 }}
                        />
                        <Text>{pokemon.nickname}</Text>
                        <Text>{pokemon.name}</Text>
                        <TouchableOpacity onPress={props.handleShare} >
                            <Text>COMPARTIR</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            Alert.alert(`Eliminar a ${pokemon.name}`, '¿Estas seguro de querer eliminar este pokemon de tu lista?', [
                                { text: 'Eliminar', onPress: () => props.handleDelete(props.keys[i]) },
                                { text: 'Cancelar' }
                            ])
                        }} >
                            <Icon name={"delete"} size={20} color="#c62828" />
                        </TouchableOpacity>
                    </View>
                ))
            }
        </View>
    </View>
)

const styles = StyleSheet.create({
    container: { flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' },
})

export default MyPokemonsList