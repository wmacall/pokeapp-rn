import React, { Component } from "react";
import { Text, View, TextInput, SafeAreaView, KeyboardAvoidingView, TouchableOpacity, Alert } from "react-native";
import { styles } from "./register.styles";
import firebase from "firebase";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class Register extends Component {

    static navigationOptions = {
        header: null
    }

    state = { email: '', password: '', showPass: true }

    handleGoToHome = () => {
        this.props.navigation.navigate('Home')
    }

    handleBackToLogin = () => {
        this.props.navigation.navigate('LoginScreen')
    }

    signInFacebook = () => {
        const provider = new firebase.auth.GoogleAuthProvider()
        firebase.auth().signInWithPopup(provider).then((result) => {
            let token = result.credential.accessToken
            console.warn(result.user)
        }).catch(error => {
            console.warn(error.provider)
        })
    }

    handleNewUser = () => {
        const { email, password } = this.state
        if (email != "" && password != "") {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            Alert.alert('Registro completado', '¡Bienvenido!', [
                { text: 'Aceptar', onPress: this.handleGoToHome }
            ], { cancelable: false })
        } else {
            Alert.alert('Campos Incompletos', 'Debes de llenar todos los campos', [{ text: 'Aceptar' }])
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.container} >
                <KeyboardAvoidingView behavior="padding" enabled >
                    <View style={styles.containerTitle}  >
                        <Text style={styles.titleBox} >Ingresa los datos</Text>
                        <Text><Icon name="pokeball" size={30} color="#BF360C" /></Text>
                    </View>
                    <View style={styles.inputContainer} >
                        <TextInput
                            underlineColorAndroid="transparent" placeholder="Ingresa tu correo"
                            keyboardType="email-address" style={styles.inputText} style={{ textAlign: 'center' }}
                            onChangeText={email => this.setState({ email: email })} />
                    </View>
                    <View style={styles.inputContainer} >
                        <TextInput
                            underlineColorAndroid="transparent" placeholder="Contraseña"
                            keyboardType="default" secureTextEntry={this.state.showPass} style={{ textAlign: 'center' }}
                            onChangeText={password => this.setState({ password: password })} />
                    </View>
                </KeyboardAvoidingView>
                <View style={{ margin: 10 }} >
                    <Icon.Button name="facebook" backgroundColor="#3b5998" onPress={this.signInFacebook} >
                        <Text style={styles.textIcon}>
                            Registrase con Facebook
                            </Text>
                    </Icon.Button>
                </View>
                <View style={{ margin: 10 }} >
                    <Icon.Button name="login" backgroundColor="#e53935" onPress={this.handleNewUser}  >
                        <Text style={styles.textIcon}>
                            Registrase
                            </Text>
                    </Icon.Button>
                </View>
                <TouchableOpacity style={styles.containerFooter} onPress={this.handleBackToLogin} >
                    <Text style={styles.backButton} >Regresar</Text>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}