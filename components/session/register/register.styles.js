import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    containerTitle: { marginTop: 20, alignItems: 'center', padding: 10 },
    titleBox: { textAlign: 'center', padding: 10, fontSize: 14, letterSpacing: 2, fontWeight: 'bold', color: '#424242', marginTop: 4, borderRadius: 50, borderColor: '#000', borderWidth: .2, width: 200 },
    inputContainer: { borderRadius: 10, borderColor: '#1E88E5', borderWidth: 2, margin: 5, width: 200 },
    textIcon: { fontFamily: 'Arial', fontSize: 15, color: '#fff' },
    containerFooter: { display: 'flex', justifyContent: 'center', marginTop: 20, height: 50, padding: 4, alignItems: 'center' },
    backButton: { textAlign: 'center', padding: 10, fontSize: 14, letterSpacing: 2, color: '#e53935', borderRadius: 50, backgroundColor: '#fff', borderWidth: .4, width: 100, borderColor: '#e53935' },
})

export { styles }