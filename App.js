import React from "react";
import { StyleSheet, Text, View, Image, SafeAreaView, TouchableOpacity, KeyboardAvoidingView, TextInput, Alert } from 'react-native';
import { createSwitchNavigator, createAppContainer, createStackNavigator, createBottomTabNavigator, createDrawerNavigator } from "react-navigation";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import firebase from './config'
//Components
import Pokemons from "./components/pokemons";
import PokemonDetail from "./components/pokemons/PokemonDetail";
import Register from "./components/session/register/Register";
import MyPokemons from "./components/my-pokemons/MyPokemons";
import SearchUser from "./components/user/SearchUser";
class LoginScreen extends React.Component {

    constructor(...props) {
        super(...props)
        this.state = { showPass: true, email: '', password: '', isLogged: false }
    }

    componentDidMount() {
        this.removeListener = firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.setState({ isLogged: true })
                this.props.navigation.navigate('Home')
            }
        })
    }

    componentWillUnmount() {
        this.removeListener()
    }

    handleGoToPokemons = () => {
        this.props.navigation.navigate('App')
    }

    handleLoginUser = () => {
        const { email, password } = this.state
        if (email != "" && password != "") {
            firebase.auth().signInWithEmailAndPassword(email, password)
                .catch(error => {
                    console.warn(error)
                })
        } else {
            Alert.alert('Campos Incompletos', 'Debes de llenar todos los campos', [
                { text: 'Aceptar' }
            ])
        }
        this.setState({ isLogged: true })
        this.handleGoToPokemons()
    }

    render() {
        return (
            <SafeAreaView style={styles.container} >
                <KeyboardAvoidingView behavior="padding" enabled  >
                    <View style={{ alignItems: 'center', padding: 5 }} >
                        <Image source={require('./assets/pikachu.png')} resizeMode="stretch" style={{ height: 70, width: 70 }} />
                    </View>
                    <View style={{ alignItems: 'center', padding: 20 }}>
                        <Text style={styles.titleBanner} >Iniciar Sesión</Text>
                    </View>
                    <View style={styles.inputContainer} >
                        <TextInput
                            underlineColorAndroid="transparent" placeholder="Ingresa tu correo"
                            keyboardType="email-address" style={styles.inputText} style={{ textAlign: 'center' }}
                            onChangeText={email => this.setState({ email: email })} />
                    </View>
                    <View style={styles.inputContainer} >
                        <TextInput
                            underlineColorAndroid="transparent" placeholder="Contraseña"
                            keyboardType="default" secureTextEntry={this.state.showPass} style={{ textAlign: 'center' }}
                            onChangeText={password => this.setState({ password: password })} />
                    </View>
                </KeyboardAvoidingView>
                <View style={styles.boxButtons}>
                    <TouchableOpacity style={styles.buttonContainer} onPress={() => this.handleLoginUser()} >
                        <Text style={styles.buttonText} >Vamos</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')} >
                        <Text style={{ textDecorationLine: 'underline', padding: 10 }} >Registrase</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.footer} >
                    <Text style={styles.title} >Pokemon App</Text>
                </View>
            </SafeAreaView>
        )
    }
}

const DrawerNavigator = createDrawerNavigator({
    Home: {
        screen: Pokemons
    }
})

const PokemosStack = createStackNavigator({ Home: Pokemons, Detail: PokemonDetail, })
const RegisterStack = createStackNavigator({ Register: Register })
const SearchStack = createStackNavigator({ SearchUser: SearchUser })
const RootStack = createSwitchNavigator(
    {
        LoginScreen: LoginScreen, Register: RegisterStack,Drawer: DrawerNavigator,
        Main: createBottomTabNavigator({
            'Pokemons': PokemosStack,
            'Mis Pokemons': MyPokemons,
            'Buscar Pokemons': SearchStack,
        },
        )
    }
)

const App = createAppContainer(RootStack)

const styles = StyleSheet.create({
    container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
    inputContainer: { borderRadius: 10, borderColor: '#1E88E5', borderWidth: 2, margin: 5, width: 200 },
    title: { fontWeight: 'bold', fontSize: 15, color: '#fff', fontWeight: '900', letterSpacing: 4, padding: 2 },
    titleBanner: { textAlign: 'center', borderRadius: 140, width: 140, borderColor: '#000', borderWidth: 1, padding: 5 },
    buttonContainer: { paddingVertical: 10, backgroundColor: '#b71c1c', borderRadius: 64, width: 70 },
    buttonText: { color: '#fff', textAlign: 'center', fontSize: 15, fontWeight: 'bold' },
    boxButtons: { height: 50, alignItems: 'center', padding: 5 },
    footer: { height: 50, backgroundColor: '#c62828', position: 'absolute', bottom: 0, alignItems: 'center', width: '100%', justifyContent: 'center' }

});

export default App