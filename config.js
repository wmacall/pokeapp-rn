import firebase from 'firebase'
import 'firebase/firestore'

const settings = {timestampsInSnapshots: true}
const config = {
  apiKey: "AIzaSyA49R5T-Abbkgv2DxWmpbq2KHRnn6ZJRfE",
  authDomain: "pokeapp-a97f9.firebaseapp.com",
  databaseURL: "https://pokeapp-a97f9.firebaseio.com",
  projectId: "pokeapp-a97f9",
  storageBucket: "pokeapp-a97f9.appspot.com",
  messagingSenderId: "574194570904"
};

firebase.initializeApp(config)
export default firebase